export function IsNullOrUndefined(objeto): boolean {
  if (objeto == null) return true;
  if (objeto == undefined) return true;
  return false;
}
