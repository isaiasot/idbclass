export interface iDBOptions {
  /** La propiedad keyPath define dónde el navegador buscará la llave de referencia en el ObjectStore. No puede incluir espacios ... */
  keyPath?: string;
  /** Si es true, se crea un índice auto incrementable en el ObjectStore. */
  autoIncrement?: boolean;
  /** Modo en que se creará una transacción en el ObjectStore */
  transactionMode: IDBTransactionMode;
  //crearIDB(): Promise<IDBObjectStore>;
}
