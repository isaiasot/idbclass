import * as utils from "./utils";
import { iDBOptions } from "./options";
export default class iDB {
  dbName: string;
  dbVersion: number;
  idbOptions: iDBOptions;
  /**
   * @param {string} dbName Correponde al nombre de la base de datos
   * @param {number} dbVersion Corresponde al número de versión de la base de datos
   */
  constructor(dbName: string, dbVersion: number) {
    if (utils.IsNullOrUndefined(dbName) || utils.IsNullOrUndefined(dbVersion)) {
      throw new TypeError("Los parámetros no pueden estar vacíos");
    } else {
      this.dbName = dbName;
      this.dbVersion = dbVersion;
    }
    this.idbOptions = {
      autoIncrement: false,
      keyPath: null,
      transactionMode: "readonly"
    };
  }

  /**
   * Crea una nueva base de datos indexada junto con el primer ObjectStore
   * @param {string} dbName Correponde al nombre de la base de datos
   * @param {string} objStoreName Correponde al nombre del ObjectStore que se almacenará en la base de datos
   * @param {number} dbVersion Corresponde al número de versión de la base de datos
   * @param {iDBOptions} iDBOptions Corresponde a las opciones de creación del ObjectStore
   * @return {Promise<IDBObjectStore>} El objeto IDBObjectStore con sus propiedades
   */
  createIDB(
    dbName: string = this.dbName,
    objStoreName: string = this.dbName,
    dbVersion: number = this.dbVersion,
    iDBOptions: iDBOptions = this.idbOptions
  ): Promise<IDBObjectStore> {
    try {
      return new Promise<IDBObjectStore>((resolve, reject) => {
        var req = indexedDB.open(dbName, dbVersion);
        req.onupgradeneeded = evt => {
          var db = evt.target.result;
          var objStore = db.createObjectStore(objStoreName, {
            keyPath: iDBOptions.keyPath,
            autoIncrement: iDBOptions.autoIncrement
          });
          resolve(objStore);
        };
        req.onerror = evt => {
          reject({
            mensaje: `Error al cargar la base de datos ${dbName}: ${dbVersion}.`,
            errorEvt: evt
          });
        };
        req.onsuccess = evt => {
          var db = evt.target.result;
          db.close();
        };
      });
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * Crea una nueva transacción en el objectStore y resuelve con el objeto de la transacción. Usar con add(), get(), put(), delete()
   * @param {string} dbName Correponde al nombre de la base de datos
   * @param {string} objStoreName Correponde al nombre del ObjectStore que se almacenará en la base de datos
   * @param {number} dbVersion Corresponde al número de versión de la base de datos
   * @param {iDBOptions} iDBOptions Corresponde a las opciones de creación del ObjectStore
   */
  transaction(
    dbName: string = this.dbName,
    objStoreName: string = this.dbName,
    dbVersion: number = this.dbVersion,
    iDBOptions: iDBOptions = this.idbOptions
  ) {
    try {
      return new Promise((resolve, reject) => {
        var req = indexedDB.open(dbName, dbVersion);
        req.onsuccess = evt => {
          var db = evt.target.result;
          var transaction = db.transaction(
            objStoreName,
            iDBOptions.transactionMode
          );
          var objectStore = transaction.objectStore(objStoreName);
          resolve(objectStore);
          db.close();
        };
        req.onerror = evt => {
          reject({
            mensaje: `Error al cargar la base de datos ${dbName}: ${dbVersion}.`,
            errorEvt: evt
          });
        };
      });
    } catch (error) {
      console.log(error);
    }
  }

  /**
   * Obtiene el objeto almacenado en un ObjectStore
   * @param {string} dbName Correponde al nombre de la base de datos
   * @param {string} objStoreName Correponde al nombre del ObjectStore que se almacenará en la base de datos
   * @param {number} dbVersion Corresponde al número de versión de la base de datos
   * @param {IDBValidKey} key Correponde al valor que se quiere buscar. Nota: solo busca a través de los valores en el keyPath
   */
  get(
    key: IDBValidKey,
    dbName: string = this.dbName,
    dbVersion: number = this.dbVersion,
    objStoreName?: string
  ): Promise<any> {
    if (utils.IsNullOrUndefined(dbName)) dbName = this.dbName;
    if (utils.IsNullOrUndefined(dbVersion)) dbVersion = this.dbVersion;
    if (utils.IsNullOrUndefined(objStoreName)) objStoreName = this.dbName;
    try {
      return new Promise<any>((resolve, reject) => {
        var req = indexedDB.open(dbName, dbVersion);
        req.onsuccess = evt => {
          let db = evt.target.result;
          let txObjStore = db
            .transaction(objStoreName, "readonly")
            .objectStore(objStoreName);
          let get = txObjStore.get(key);
          get.onsuccess = get => {
            resolve(get.target.result);
          };
          db.close();
        };
        req.onerror = evt => {
          reject({
            mensaje: `Error al cargar la base de datos ${dbName}: ${dbVersion}.`,
            errorEvt: evt
          });
        };
      });
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * Agrega o actualiza valores en un ObjectStore, se recomienda su uso si no se asignaron keyPath o se agregaron índices al ObjectStore. De otro modo use transaction()
   * @param {string} dbName Correponde al nombre de la base de datos
   * @param {string} objStoreName Correponde al nombre del ObjectStore que se almacenará en la base de datos
   * @param {number} dbVersion Corresponde al número de versión de la base de datos
   * @param {IDBValidKey} key Correponde a la llave del valor
   * @param {any} value Valor que se agregará al ObjectStore
   */
  set(
    key: IDBValidKey,
    value: any,
    dbName: string = this.dbName,
    dbVersion: number = this.dbVersion,
    objStoreName?: string
  ): Promise<void> {
    if (utils.IsNullOrUndefined(objStoreName)) objStoreName = this.dbName;
    try {
      return new Promise<void>((resolve, reject) => {
        var req = indexedDB.open(dbName, dbVersion);
        req.onsuccess = evt => {
          let db = evt.target.result;
          let txObjStore = db
            .transaction(objStoreName, "readwrite")
            .objectStore(objStoreName);
          txObjStore.put(value, key);
          db.close();
        };
      });
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * Elimina un registro de un Object Store de la IndexedDB
   * @param {string} dbName Correponde al nombre de la base de datos
   * @param {string} objStore Correponde al nombre del ObjectStore que se almacenará en la base de datos
   * @param {number} dbVersion Corresponde al número de versión de la base de datos
   * @param {IDBValidKey} key Correponde a la llave del valor
   */
  delete(
    key: IDBValidKey,
    dbName: string = this.dbName,
    dbVersion: number = this.dbVersion,
    objectStore: string = this.dbName
  ): Promise<void> {
    try {
      return new Promise<void>((resolve, reject) => {
        var req = indexedDB.open(dbName, dbVersion);
        req.onsuccess = evt => {
          let db = evt.target.result;
          let txObjStore = db
            .transaction(objectStore, "readwrite")
            .objectStore(objectStore);
          txObjStore.delete(key);
          txObjStore.onsuccess = evt => {
            resolve();
          };
          db.close();
        };
        req.onerror = evt => {
          reject({
            mensaje: `Error al cargar la base de datos ${dbName}: ${dbVersion}.`,
            errorEvt: evt
          });
        };
      });
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * Elimina el objectStore de una base de datos indexada
   * @param {string} dbName Correponde al nombre de la base de datos
   * @param {string} objStoreName Correponde al nombre del ObjectStore que se almacenará en la base de datos
   * @param {number} dbVersion Corresponde al número de versión de la base de datos
   * @return {Promise<boolean>} Booleano que indica que se eliminó correctamente el objectStore
   */
  deleteObjStore(
    objStoreName: string,
    dbName?: string,
    dbVersion?: number
  ): Promise<boolean> {
    if (utils.IsNullOrUndefined(dbName)) dbName = this.dbName;
    if (utils.IsNullOrUndefined(dbVersion)) dbVersion = this.dbVersion;
    if (utils.IsNullOrUndefined(objStoreName)) objStoreName = this.dbName;
    try {
      return new Promise<boolean>((resolve, reject) => {
        var req = indexedDB.open(dbName, dbVersion);
        req.onupgradeneeded = evt => {
          var db = evt.target.result;
          db.deleteObjectStore(objStoreName);
          resolve(true);
        };
        req.onerror = evt => {
          reject({
            mensaje: `Error al cargar la base de datos ${dbName}: ${dbVersion}.`,
            errorEvt: evt
          });
        };
        req.onsuccess = evt => {
          var db = evt.target.result;
          db.close();
        };
      });
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * Elimina una base de datos indexada
   * @param {string} dbName Correponde al nombre de la base de datos
   * @return {Promise<void>} Booleano que indica que se eliminó correctamente la base de datos indexada
   */
  deleteIDB(dbName: string = this.dbName): Promise<void> {
    try {
      return new Promise<void>((resolve, reject) => {
        var req = indexedDB.deleteDatabase(dbName);
        req.onsuccess = evt => {
          resolve();
        };
        req.onerror = evt => {
          reject({
            mensaje: `Error al cargar la base de datos ${dbName}.`,
            errorEvt: evt
          });
        };
      });
    } catch (error) {
      console.error(error);
    }
  }
}
